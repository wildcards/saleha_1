
<?php   include_once('happy_client.php'); ?>

<footer>

    <div class="container">
        <div class="footer_top">

        </div>
            <div class="footer_bottom">.
                <p class="copyright_text"> Copy Right &copy; By <a href="#"> CodeMasons </a> of BITM </p>
            </div>
    </div>

</footer>
<a href="#" class="scrollToTop"> </a>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../../../../resource/bootstrap/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../../resource/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../../resource/js/wow.min.js"></script>
<script>
    new WOW().init();
</script>
<script>

    $(document).ready(function(){
        //Check to see if the window is top if not then display button
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
            }
        });
        //Click event to scroll to top
        $('.scrollToTop').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });

    });
</script>
</body>
</html>